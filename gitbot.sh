#!/bin/sh

help()
{
	/usr/bin/echo "Usage: gitbot [option] [path-to-local-repository]

NOTE: This program needs a valid SSH keypair to operate without being stuck in a password prompt.

OPTIONS:
	- push: commit local changes and push to remote repository
	- pull: pull changes from remote repository

ENVIRONMENT VARIABLES:
	- COMMITMSG: commit message to include when 'push' option is used. Defaults to \"This commit was done automatically\". Value must be between double-quotes (\" \").
	- REMOTE: name of the remote to push to/pull from. Defaults to 'origin'.
	- BRANCH: branch to push to/pull from. Defaults to 'master'.
	- NOTIFY: true/false value. Indicates if gitbot should send you notifications or not"
}

notify() {
	if [ $NOTIFY == true ]
	then
		/usr/bin/notify-send $1 &
    fi
}

if [ -z $1 ] || [ -z $2 ]
then
	help
	exit 1
else
	MODE=$1
	REPO=$2
fi

cd $REPO

if [ -z $COMMITMSG ]
then
	COMMITMSG="This commit was done automatically"
fi

if [ -z $REMOTE ]
then
	REMOTE=origin
fi

if [ -z $BRANCH ]
then
	BRANCH=master
fi

if [ -z $NOTIFY ]
then
	NOTIFY=true
fi

if [ $MODE == "push" ]
then
	/usr/bin/git commit -am "$COMMITMSG"
	/usr/bin/git push $REMOTE $BRANCH
	notify "Update done successfully"
	exit 1
elif [ $MODE == "pull" ]
then
	/usr/bin/git pull $REMOTE $BRANCH
	notify "Pulled changes from ${REMOTE} to ${REPO}"
	exit 1
else
	help
	exit 1
fi

### gitbot
### Copyright (C) 2021  Miskolczi Richárd
### 
### This program is free software; you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation; either version 2 of the License, or
### (at your option) any later version.
### 
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
### 
### You should have received a copy of the GNU General Public License along
### with this program; if not, write to the Free Software Foundation, Inc.,
### 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###
### Full license: /usr/share/licenses/gitbot/LICENSE
### Contact: <miskolczi.richard@protonmail.com>
